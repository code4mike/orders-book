import { Provider } from 'react-redux';
import { useState } from 'react';

import store from './store';
import OrderBook from './OrderBook';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import lightPalette from './themes/light';
import darkPalette from './themes/dark';
import sepiaPalette from './themes/sepia';

function App() {
  const [theme, setTheme] = useState(darkPalette);
  const themes = {
    'light': lightPalette,
    'dark': darkPalette,
    'sepia': sepiaPalette
  };

  const handleThemeChange = (themeName) => {
    setTheme(themes[themeName])
  }

  return (
    <div className="App">
      <MuiThemeProvider theme={createMuiTheme(theme)}>
        <Provider store={store}>
          <OrderBook onThemeChange={handleThemeChange}/>
        </Provider>
      </MuiThemeProvider>
    </div>
  );
}

export default App;
