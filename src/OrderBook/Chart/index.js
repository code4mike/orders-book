import { Box, withStyles } from "@material-ui/core";
import { connect } from "react-redux";
import constants from "../../constants";

const Styles = (theme) => ({
  chart: {
    display: 'flex',
    position: 'absolute',
    height: '100%',
    width: '100%',
    opacity: 0.3,
    overflow: 'hidden'
  },
  bars: {
    width: '50%',
    height: '100%',
    display: 'flex',
    justifyContent: 'space-evenly',
    transform: 'rotate(180deg)'
  },
  bidBars: {
    flexDirection: 'row-reverse'
  },
  bidBar: {
    width: '100%',
    backgroundColor: theme.palette.success.main
  },
  askBar: {
    width: '100%',
    backgroundColor: theme.palette.error.main
  }
})

const Chart = ({ classes, bidOrders, askOrders, depthVis }) => {

  const bidBars = (orders) => {
    const result = []
    if (orders.length) {
      const maxTotal = orders[Math.min(orders.length - 1, 24)].total
      for (let i = 0; i < 25; i++) {
        if (orders[i]) {
          let percentVal
          if (depthVis === constants.CUMULATIVE_DEPTH_VIS) {
            percentVal = (orders[i].total * 100) / maxTotal;
          } else {
            percentVal = (orders[i].amount * 100) / maxTotal;
          }
          result.push(
            <Box className={classes.bidBar} key={`bidBar${i}`} style={{ height: percentVal + '%' }}></Box>
          )
        }
      }
    }
    return result;
  }

  const askBars = (orders) => {
    const result = []
    if (orders.length) {
      const maxTotal = orders[Math.min(orders.length - 1, 24)].total
      for (let i = 0; i < 25; i++) {
        if (orders[i]) {
          let percentVal
          if (depthVis === constants.CUMULATIVE_DEPTH_VIS) {
            percentVal = (orders[i].total * 100) / maxTotal;
          } else {
            percentVal = (orders[i].amount * 100) / maxTotal;
          }

          result.push(
            <Box className={classes.askBar} key={`askBar${i}`} style={{ height: percentVal + '%' }}></Box>
          )
        }
      }
    }
    return result;
  }

  return (
    <Box className={classes.chart}>
      <Box className={`${classes.bars} ${classes.bidBars}`}>
        {bidBars(bidOrders)}
      </Box>
      <Box className={`${classes.bars} ${classes.askBars}`}>
        {askBars(askOrders)}
      </Box>
    </Box>
  )
}

const mapStateToProps = (state) => ({
  depthVis: state.depthVis
})

export default withStyles(Styles)(connect(mapStateToProps)(Chart));