import { connect } from 'react-redux';
import {
  withStyles,
  Box,
  Button,
  Chip,
  Typography
} from '@material-ui/core';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import SettingsIcon from '@material-ui/icons/Settings';
import SettingsModal from '../SettingsModal';
import constants from '../../constants';

import {
  decreasePrecision,
  increasePrecision
} from '../actions';
import { useState } from 'react';

const Styles = (theme) => ({
  button: {
    background: 'none',
    color: theme.palette.text.primary,
    border: 'none',
    cursor: 'pointer',
    padding: 5,
    width: 26,
    height: 16,
    minWidth: 'unset',
    '&[disabled]': {
      color: theme.palette.grey[50],
    }
  },
  themes: {
    fontSize: 10,
    display: 'inline',
    marginRight: 5,
    marginLeft: 20
  },
  lightChip: {
    backgroundColor: '#ffffff',
    color: '#000000',
    marginRight: 5
  },
  darkChip: {
    backgroundColor: '#000000',
    color: '#ffffff',
    marginRight: 5
  },
  sepiaChip: {
    backgroundColor: '#ccc0ab',
    color: '#000000'
  }
});

const ChartControls = ({ classes, precision, decreasePrecision, increasePrecision, onThemeChange }) => {
  const [settingsModalOpen, setSettingsModalOpen] = useState(false);
  const handleIncrease = () => {
    const data = {
      precision: ++precision
    };
    increasePrecision(data);
  }
  
  const handleDecrease = () => {
    const data = {
      precision: --precision
    };
    decreasePrecision(data);
  }

  const toggleSettingsModal = () => {
    setSettingsModalOpen(!settingsModalOpen)
  }

  return (
    <Box className="chartControls">
      <Button
        title="Decrease Precision"
        className={classes.button}
        disabled={precision === constants.MIN_PRECISION}
        onClick={handleDecrease}
      >
        <RemoveIcon fontSize="small" />
      </Button>
      <Button
        title="Increase Precision"
        className={classes.button}
        disabled={precision === constants.MAX_PRECISION}
        onClick={handleIncrease}
      >
        <AddIcon fontSize="small" />
      </Button>
      <Button
        title="Chart Settings"
        className={classes.button}
        onClick={toggleSettingsModal}
      >
        <SettingsIcon fontSize="small" />
      </Button>
      <Typography className={classes.themes}>Themes:</Typography>
      <Chip size="small" label="L" onClick={() => onThemeChange('light')} className={classes.lightChip} />
      <Chip size="small" label="D" onClick={() => onThemeChange('dark')} className={classes.darkChip} />
      <Chip size="small" label="S" onClick={() => onThemeChange('sepia')} className={classes.sepiaChip} />
      <SettingsModal open={settingsModalOpen} handleClose={toggleSettingsModal} />
    </Box>
  )
}

const mapStateToProps = (state) => {
  return {
    precision: state.precision
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    decreasePrecision: (data) => dispatch(decreasePrecision(data)),
    increasePrecision: (data) => dispatch(increasePrecision(data))
  };
}

export default withStyles(Styles)(connect(
  mapStateToProps,
  mapDispatchToProps)(ChartControls));