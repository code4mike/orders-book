import { Box, Typography, withStyles } from "@material-ui/core";
import RefreshIcon from '@material-ui/icons/Cached';
import { connect } from "react-redux";
import constants from "../../../constants";

const Styles = (theme) => ({
  wrapper: {
    position: 'relative',
    height: '100%'
  },
  row: {
    display: 'flex',
    justifyContent: 'space-evenly',
    position: 'relative'
  },
  highlightBar: {
    height: '100%',
    position: 'absolute',
    top: 0,
    opacity: 0.3
  },
  bidHighlightBar: {
    right: 0,
    backgroundColor: theme.palette.success.main
  },
  askHighlightBar: {
    left: 0,
    backgroundColor: theme.palette.error.main
  },
  cell: {
    width: '25%',
    textAlign: 'center',
    textTransform: 'capitalize'
  },
  loadingWrapper: {
    position: 'absolute',
    top: '25%',
    left: '50%',
    transform: 'translateX(-50%)',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  }
})

const TableBody = ({ classes, loading, type, values, fieldOrder, maxTotal, chartOrientation, depthVis }) => {
  const generateRows = () => {
    const result = []
    for (let i = 0; i < 25; i++) {
      if (values[i]) {
        let percentVal = 0;
        if (depthVis === constants.CUMULATIVE_DEPTH_VIS) {
          percentVal = (values[i].total * 100) / maxTotal;
        } else {
          percentVal = (values[i].amount * 100) / maxTotal;
        }
        result.push(
          <div className={classes.row} key={`${type}Order${i}`}>
            <div className={classes.cell}>{values[i][fieldOrder[0]]}</div>
            <div className={classes.cell}>{values[i][fieldOrder[1]]}</div>
            <div className={classes.cell}>{values[i][fieldOrder[2]]}</div>
            <div className={classes.cell}>{values[i][fieldOrder[3]]}</div>
            {chartOrientation === constants.HORIZONTAL_CHART && (
              <div
                className={`${classes.highlightBar} ${classes[`${type}HighlightBar`]}`}
                style={{width: `${percentVal}%`}}
              ></div>
            )}
          </div>
        )
      }
    }
    return result;
  }

  return (
    <Box className={classes.wrapper}>
      {!loading && generateRows()}
      {loading && (
        <Box className={classes.loadingWrapper}>
          <Typography>Loading</Typography>
          <RefreshIcon className="refreshIcon" />
        </Box>
      )}
    </Box>
  )
}
const mapStateToProps = state => ({
  chartOrientation: state.chartOrientation,
  depthVis: state.depthVis
})

export default withStyles(Styles)(connect(mapStateToProps)(TableBody))