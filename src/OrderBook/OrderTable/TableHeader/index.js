import { Box, withStyles } from '@material-ui/core';
import React from 'react';

const Styles = (theme) => ({
  headerWrapper: {
    display: 'flex',
    justifyContent: 'space-evenly',
    color: theme.palette.text.secondary
  },
  cell: {
    width: '25%',
    textAlign: 'center',
    textTransform: 'capitalize'

  },
  countCell: {
    width: '25%',
  },
  amountCell: {
    width: '25%',
    textAlign: 'center'
  },
  totalCell: {
    width: '25%',
    textAlign: 'center'
  },
  priceCell: {
    width: '25%',
    textAlign: 'center'
  }
})

const TableHeader = React.memo(({classes, headerNames}) => {
    return (
      <Box className={classes.headerWrapper}>
        {headerNames?.map(headerName => (
          <Box className={classes.cell} key={headerName}>{headerName}</Box>
        ))}
      </Box>
    )
})

export default withStyles(Styles)(TableHeader);