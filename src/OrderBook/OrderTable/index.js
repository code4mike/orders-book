import { Box, withStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import TableBody from './TableBody';
import TableHeader from './TableHeader';

const Styles = () => ({
  orders: {
    width: '50%'
  }
});

const OrderTable = ({ classes, type, headerNames, orderValues, loading }) => {
  const maxTotal = orderValues && orderValues.length ? orderValues[Math.min(orderValues.length - 1, 24)].total : 0;
  return (
    <Box className={classes.orders}>
      <TableHeader headerNames={headerNames} />
      <TableBody loading={loading} type={type} values={orderValues} fieldOrder={headerNames} maxTotal={maxTotal} />
    </Box>
  )
}

const mapStateToProps = (state) => ({
  loading: state.loading
})

export default withStyles(Styles)(connect(
  mapStateToProps
)(OrderTable));