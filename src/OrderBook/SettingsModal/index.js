import { useState } from 'react';
import { connect } from 'react-redux';
import {
  Box,
  DialogContent,
  DialogTitle,
  RadioGroup,
  Radio,
  Typography,
  withStyles,
  FormControlLabel
} from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import { Close } from '@material-ui/icons';

import constants from '../../constants';
import * as config from '../../config.json';
import { changeColumnOrder, changeChartOrientation, changeDepthVis } from '../actions';

const Styles = (theme) => ({
  closeWrapper: {
    position: 'absolute',
    right: 5,
    top: 5
  },
  closeIcon: {
    cursor: 'pointer'
  },
  dialog: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.text.primary,
    padding: '0 20px',
    width: 600,
    height: 400
  },
  dialogTitle: {
    padding: '25px 0px 15px',
    borderBottom: `${theme.palette.text.secondary} 1px solid`
  },
  dialogContent: {
    padding: '10px 0'
  },
  bidsLabel: {
    display: 'inline',
    color: theme.palette.success.main,
    textTransform: 'capitalize'
  },
  asksLabel: {
    display: 'inline',
    color: theme.palette.error.main,
    textTransform: 'capitalize'
  },
  formControlLabel: {
    height: 30,
    marginLeft: 5
  },
  controlsWrapper: {
    marginBottom: 10
  }
})

const ColumnNameLabel = ({ classes, columnOrder }) => (
  <Box>
    <Typography className={classes.bidsLabel}>{columnOrder.bids.map(val => ` ${val}`)}</Typography>
    <Typography className={classes.asksLabel}>{columnOrder.asks.map(val => ` ${val}`)}</Typography>
  </Box>
)

const SettingsModal = ({
  classes, open, handleClose,
  columnOrder, changeColumnOrder,
  chartOrientation, changeChartOrientation,
  depthVis, changeDepthVis
}) => {

  const handleColumnOrderChange = (e) => {
    changeColumnOrder({value: e.target.value})
  }

  const handleOrientationChange = (e) => {
    changeChartOrientation({value: e.target.value})
  }

  const handleDepthVisChange = (e) => {
    changeDepthVis({value: e.target.value});
  }

  return (
    <Dialog
      open={open}
      PaperProps={{
        className: classes.dialog
      }}>
      <Box className={classes.closeWrapper}><Close fontSize="small" onClick={handleClose} className={classes.closeIcon} /></Box>
      <DialogTitle className={classes.dialogTitle}>
        <Typography variant="h5">Interface settings for order book</Typography>
      </DialogTitle>
      <DialogContent className={classes.dialogContent}>
        <Box className={classes.controlsWrapper}>
          <Typography>Choose chart orientation:</Typography>
          <RadioGroup name="chartOrientation" value={`${chartOrientation}`} onChange={handleOrientationChange}>
            <FormControlLabel
              className={classes.formControlLabel}
              value={`${constants.HORIZONTAL_CHART}`}
              control={<Radio />}
              label="Horizontal"
            />
            <FormControlLabel
              className={classes.formControlLabel}
              value={`${constants.VERTICAL_CHART}`}
              control={<Radio />}
              label="Vertical"
            />
          </RadioGroup>
        </Box>
        <Box className={classes.controlsWrapper}>
          <Typography>Book Depth Visualization:</Typography>
          <RadioGroup name="depthVis" value={`${depthVis}`} onChange={handleDepthVisChange}>
            <FormControlLabel
              className={classes.formControlLabel}
              value={`${constants.CUMULATIVE_DEPTH_VIS}`}
              control={<Radio />}
              label="Cummulative"
            />
            <FormControlLabel
              className={classes.formControlLabel}
              value={`${constants.AMOUNT_DEPTH_VIS}`}
              control={<Radio />}
              label="Amount"
            />
          </RadioGroup>
        </Box>
        <Box>
          <Typography>Choose the order of the columns in the order book:</Typography>
          <RadioGroup name="columnOrder" value={`${columnOrder}`} onChange={handleColumnOrderChange}>
            {config.app.settings.columnOrder.map((columnOrder, index) => (
              <FormControlLabel
                key={`coulumnOrderOption${index}`}
                className={classes.formControlLabel}
                value={`${index}`}
                control={<Radio />}
                label={<ColumnNameLabel classes={classes} columnOrder={columnOrder} />}
              />
            ))}
          </RadioGroup>
        </Box>
      </DialogContent>
    </Dialog>
  )
}

const mapStateToProps = (state) => ({
  columnOrder: state.columnOrder,
  chartOrientation: state.chartOrientation,
  depthVis: state.depthVis
})

const mapDispatchToProps = (dispatch) => ({
  changeColumnOrder: (data) => dispatch(changeColumnOrder(data)),
  changeChartOrientation: (data) => dispatch(changeChartOrientation(data)),
  changeDepthVis: (data) => dispatch(changeDepthVis(data))
})

export default withStyles(Styles)(connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsModal));