import constants from '../constants';

export function fetchData() {
  return {type: constants.FETCH_DATA};
}

export function decreasePrecision(data) {
  return {type: constants.DECREASE_PRECISION, data};
}

export function increasePrecision(data) {
  return {type: constants.INCREASE_PRECISION, data};
}

export function loadingComplete() {
  return {type: constants.LOADING_COMPLETE};
}

export function changeColumnOrder(data) {
  return {type: constants.CHANGE_COLUMN_ORDER, data}
}

export function changeChartOrientation(data) {
  return {type: constants.CHANGE_CHART_ORIENTATION, data}
}

export function changeDepthVis(data) {
  return {type: constants.CHANGE_CHART_VISULIZATION, data}
}