import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { withStyles, Box, MuiThemeProvider, createMuiTheme } from '@material-ui/core';

import ChartControls from './ChartControls';
import './styles.scss'

import {
  fetchData
} from './actions';
import Chart from './Chart';
import OrderTable from './OrderTable';
import constants from '../constants';

const Styles = (theme) => ({
  container: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.text.primary,
    margin: 40,
    padding: 10
  },
  titleWrapper: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  title: {
    borderBottom: `${theme.palette.text.secondary} solid 1px`,
    paddingBottom: 5,
    fontWeight: 'bold'
  },
  category: {
    color: theme.palette.text.secondary
  },
  ordersContainer: {
    display: 'flex',
    justifyContent: 'space-evenly',
    width: '100%',
    height: 575,
    position: 'relative',
  }
});

function OrderBook({ classes, orders, tableHeaders, fetchData, onThemeChange, chartOrientation }) {
  const [askOrders, setAskOrders] = useState([]);
  const [bidOrders, setBidOrders] = useState([]);

  const arrayDeepCopy = (arr => arr.map(order => Object.assign([], order)));

  useEffect(() => {
    /**
     * This following line invokes the websocket initially
     */
    fetchData();
  }, [fetchData]);

  useEffect(() => {
    setAskOrders(arrayDeepCopy(orders.asks));
    setBidOrders(arrayDeepCopy(orders.bids));
  }, [orders]);


  return (
    <>
      <Box className={classes.container}>
        <Box className={classes.titleWrapper}>
          <Box className={classes.title}> ORDER BOOK  <span className={classes.category}>BTC/USD</span></Box>
          <ChartControls onThemeChange={onThemeChange} />
        </Box>
        <Box className={classes.ordersContainer}>
          <OrderTable type="bid" headerNames={tableHeaders.bids} orderValues={bidOrders} />
          <OrderTable type="ask" headerNames={tableHeaders.asks} orderValues={askOrders} />
          {chartOrientation === constants.VERTICAL_CHART && (
            <Chart bidOrders={bidOrders} askOrders={askOrders}/>
          )}
        </Box>
      </Box>
    </>
  )
}

const mapStateToProps = (state) => {
  return {
    title: state.title,
    orders: state.orders,
    tableHeaders: state.tableHeaders,
    chartOrientation: state.chartOrientation
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: () => dispatch(fetchData())
  }
}

export default withStyles(Styles)(connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderBook));