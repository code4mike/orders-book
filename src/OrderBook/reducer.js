import * as config from '../config.json';
import constants from '../constants';

const initialOrders = { asks: [], bids: [] };
const initialOrdersMap = { asks: {}, bids: {} };
const initialState = {
  loading: true,
  precision: 5,
  chartOrientation: constants.HORIZONTAL_CHART,
  depthVis: constants.CUMULATIVE_DEPTH_VIS,
  columnOrder: 0,
  tableHeaders: {
    bids: config.app.settings.columnOrder[0].bids,
    asks: config.app.settings.columnOrder[0].asks
  },
  orders: initialOrders,
  ordersMap: initialOrdersMap
};

const ordersReducer = function (state = initialState, action) {
  let response
  switch (action.type) {
    case constants.CHANGE_COLUMN_ORDER: {
      const tableHeaders = {
        bids: config.app.settings.columnOrder[action.data.value].bids,
        asks: config.app.settings.columnOrder[action.data.value].asks
      }
      return {...state, tableHeaders, columnOrder: action.data.value};
    }

    case constants.CHANGE_CHART_ORIENTATION: {
      return {...state, chartOrientation: action.data.value}
    }

    case constants.CHANGE_CHART_VISULIZATION: {
      return {...state, depthVis: action.data.value}
    }

    case constants.DECREASE_PRECISION: {
      sessionStorage.removeItem(constants.ORDERS_MAP_KEY)
      const newState = {
        ...state,
        loading: true,
        orders: initialOrders,
        ordersMap: initialOrdersMap,
        precision: action.data.precision
      };
      return newState;
    }

    case constants.INCREASE_PRECISION: {
      sessionStorage.removeItem(constants.ORDERS_MAP_KEY)
      const newState = {
        ...state,
        loading: true,
        orders: initialOrders,
        ordersMap: initialOrdersMap,
        precision: action.data.precision
      };
      return newState;
    }

    case constants.ORDER_BOOK_DATA:
      response = buildOrders(state, action.data);
      sessionStorage.setItem(constants.ORDERS_MAP_KEY, JSON.stringify(response[1]))
      return { ...state, orders: response[0], ordersMap: response[1], loading: false }

    default:
      return state;
  }
}

function buildOrders(state, data) {
  const ordersMap = {
    asks: { ...state.ordersMap.asks },
    bids: { ...state.ordersMap.bids }
  };

  if (Object.keys(state.ordersMap.asks).length === 0) {
    // Try to fetch data from local store
    let persistedMap = sessionStorage.getItem(constants.ORDERS_MAP_KEY)
    if (persistedMap) {
      persistedMap = JSON.parse(persistedMap)
      ordersMap.asks = { ...persistedMap.asks }
      ordersMap.bids = { ...persistedMap.bids }
    }
  }

  if (Array.isArray(data)) {
    let orderDetails
    if (Array.isArray(data[1]) && data[1].length === 3) {
      // New individual order data from web socket
      orderDetails = data[1]
      categorizeOrders(ordersMap, orderDetails)
    } else if (Array.isArray(data[1])) {
      // First time data of 25 asks and 25 bids
      data[1].forEach(orderDetails => categorizeOrders(ordersMap, orderDetails))
    }
  }
  // const asks = Object.values(ordersMap.asks).splice(-25);
  // const bids = Object.values(ordersMap.bids).reverse().splice(0, 25);
  const asks = Object.values(ordersMap.asks);
  const bids = Object.values(ordersMap.bids).reverse();
  return [{
    asks: calculateTotal(asks),
    bids: calculateTotal(bids)
  },
    ordersMap];
}

function categorizeOrders(ordersMap, orderDetails) {
  let orderPrice = orderDetails[0]
  if (orderDetails[2] > 0) {
    // Bid order if amount is > 0
    if (ordersMap.bids[orderDetails[0]]) {
      if (orderDetails[1] === 0) {
        // Remove order if count of particular price goes to 0
        delete ordersMap.bids[orderDetails[0]]
      } else {
        // Update existing order price
        ordersMap.bids[orderDetails[0]].amount = orderDetails[2].toString().substring(0, 6)
        ordersMap.bids[orderDetails[0]].count = orderDetails[1];
      }
    } else {
      // Add new order price
      ordersMap.bids[orderDetails[0]] = {
        price: orderDetails[0],
        count: orderDetails[1],
        amount: orderDetails[2].toString().substring(0, 6)
      };
    }
  } else {
    // Ask order if amount is < 0
    if (ordersMap.asks[orderDetails[0]]) {
      if (orderDetails[1] === 0) {
        // Remove order if count of particular price goes to 0
        delete ordersMap.asks[orderDetails[0]]
      } else {
        // Update existing order price
        ordersMap.asks[orderDetails[0]].amount = Math.abs(orderDetails[2]).toString().substring(0, 6)
        ordersMap.asks[orderDetails[0]].count = orderDetails[1];
      }
    } else {
      // Add new order price
      ordersMap.asks[orderDetails[0]] = {
        price: orderDetails[0],
        count: orderDetails[1],
        amount: Math.abs(orderDetails[2]).toString().substring(0, 6)
      };
    }
  }
  return orderPrice
}

function calculateTotal(orders) {
  let total = 0;
  orders.map(order => {
    total += parseFloat(order.amount);
    order.total = total.toString().substring(0, 6)
    return order
  })
  return orders
}

export default ordersReducer;