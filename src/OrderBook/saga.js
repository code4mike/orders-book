import { put, call, take, takeLatest } from 'redux-saga/effects';
import { eventChannel } from "redux-saga";

import constants from '../constants';

let webSocket;

function initWebSocket(args) {
  const channelDetails = { 
    event: 'subscribe', 
    channel: 'book', 
    symbol: 'tBTCUSD',
    len: 25,
    prec: constants.PRECISIONS[5]
  };

  if (args && args[0]) {
    channelDetails.prec = constants.PRECISIONS[args[0].precision];
  }

  return eventChannel(emitter => {
    webSocket = new WebSocket('wss://api-pub.bitfinex.com/ws/2')

    webSocket.onopen = () => {
      console.log('Socket is Open.');
      webSocket.send(JSON.stringify(channelDetails));
    }

    webSocket.onerror = (e) => {
      console.log('Error in Socket:', e)
    }

    webSocket.onmessage = (msg) => {
      let response = null;
      try {
        response = JSON.parse(msg.data);
      } catch(e) {
        console.error(`Error while parsing : ${msg.data}`)
      }

      if (response) {
        // Trigger response to ordersReducer
        return emitter({type: constants.ORDER_BOOK_DATA, data: response})
      }
    }

    return () => {
      console.log('Socket is closed')
    }
  });
}

export default function* orderBookSaga() {
  yield takeLatest(constants.FETCH_DATA, fetchData)
  yield takeLatest(constants.INCREASE_PRECISION, increasePrecision)
  yield takeLatest(constants.DECREASE_PRECISION, decreasePrecision)
}

export function* fetchData() {
  const channel = yield call(initWebSocket)

  while(true) {
    const action = yield take(channel)
    yield put(action)
  }
}

export function* increasePrecision(payload) {
  const params = {
    precision: payload.data.precision
  }
  webSocket.close();
  const channel = yield call(initWebSocket, [params])

  while(true) {
    const action = yield take(channel)
    yield put(action)
  }
}

export function* decreasePrecision(payload) {
  const params = {
    precision: payload.data.precision
  }
  webSocket.close();
  const channel = yield call(initWebSocket, [params])

  while(true) {
    const action = yield take(channel)
    yield put(action)
  }
}