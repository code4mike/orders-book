const constants = {
  FETCH_DATA: 'FETCH_DATA',
  DECREASE_PRECISION: 'DECREASE_PRECISION',
  DECREASE_PRECISION_LOADED: 'DECREASE_PRECISION_LOADED',
  INCREASE_PRECISION: 'INCREASE_PRECISION',
  INCREASE_PRECISION_LOADED: 'INCREASE_PRECISION_LOADED',
  ORDER_BOOK_DATA: 'ORDER_BOOK_DATA',
  CHANGE_COLUMN_ORDER: 'CHANGE_COLUMN_ORDER',
  CHANGE_CHART_ORIENTATION: 'CHANGE_CHART_ORIENTATION',
  CHANGE_CHART_VISULIZATION: 'CHANGE_CHART_VISULIZATION',
  ORDERS_MAP_KEY: 'ordersMap',
  MAX_PRECISION: 5,
  MIN_PRECISION: 1,
  PRECISIONS:{
    5: 'P0',
    4: 'P1',
    3: 'P2',
    2: 'P3',
    1: 'P4',
  },
  HORIZONTAL_CHART: 'H',
  VERTICAL_CHART: 'V',
  CUMULATIVE_DEPTH_VIS: 'C',
  AMOUNT_DEPTH_VIS: 'A'
}

export default constants;