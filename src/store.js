import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import orderBookReducer from './OrderBook/reducer';
import orderBookSaga from './OrderBook/saga';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(orderBookReducer, applyMiddleware(sagaMiddleware))

sagaMiddleware.run(orderBookSaga)

export default store;