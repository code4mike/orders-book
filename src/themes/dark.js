const darkPalette = {
  palette: {
    primary: {
      main: '#273640',
    },
    text: {
      primary: '#ffffff',
      secondary: '#969b9e'
    },
    error: {
      main: '#f05359'
    },
    success: {
      main: '#01a781'
    },
    grey: {
      50: 'grey'
    }
  }
};

export default darkPalette;