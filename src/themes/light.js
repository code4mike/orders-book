const lightPalette = {
  palette: {
    primary: {
      main: '#fefefe',
    },
    text: {
      primary: '#000000',
      secondary: '#999999'
    },
    error: {
      main: '#f05359'
    },
    success: {
      main: '#01a781'
    },
    grey: {
      50: '#999999'
    }
  }
}

export default lightPalette;