const sepiaPalette = {
  palette: {
    primary: {
      main: '#ccc0ab',
    },
    text: {
      primary: '#000000',
      secondary: '#808000'
    },
    error: {
      main: '#800000'
    },
    success: {
      main: '#008080'
    },
    grey: {
      50: '#808000'
    }
  }
};

export default sepiaPalette;